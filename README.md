
Report for xresnet50 model on mos_skin_ad2_cul_all-original-4x dataset
======================================================================


This repository contains reports generated by ``Deepchecks`` - a machine learning library, for the ``MosquitoScan`` project.
    These reports evaluate the performance of an **xresnet50** model trained to classify mosquito species from dataset **mos_skin_ad2_cul_all-original-4x**.
    Reports are located in the ``pages`` folder. Each report focuses on a specific aspect of model evaluation
# Experiment settings:
  
* **Model**: xresnet50  
* **Dataset**: mos_skin_ad2_cul_all-original-4x  
* **Experiment name**: data_orig-4x-clust_spl-subs_3_v_2  
* **Run name**: xresnet50-data_orig-4x-clust_spl-subs_3_v_2-run_1  
* **Run number**: 1  
* **Link**: https://xresnet50-data-orig-4x-clust-spl-subs-3-v-2-run--59275533c8c463.gitlab.io
## Reports List:


* **Full Suite:** Combines several individual experiment reports into a single document.
    It includes sach topics as:
  - Class Performance
  - Confusion Matrix
  - Prediction Drift report
  - Data Integrity
  - Model Evaluation
  - Train Test Validation
* **Simple Model Comparison (stratified):** Compares the xresnet50 model with other basic models on a balanced dataset.
    
* **Simple Model Comparison (stratified at 0.6):** Similar to above, but focuses on a specific data threshold (0.6).
    
* **Single Dataset Performance:** Analyzes the model's performance on a single dataset.
    
## Getting Started:
  
1. Open [the link](https://xresnet50-data-orig-4x-clust-spl-subs-3-v-2-run--59275533c8c463.gitlab.io) in your web browser to view the list of reports.  
2. Click on the report link to access the detailed evaluation results.
### Note:



    This README provides a general overview. Specific details about the experiment setup, data,
    and model hyperparameters may be found within the individual reports.